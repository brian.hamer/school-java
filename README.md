# Java Challenge - School

## Tools:
    -IntelliJ IDEA
    -Postman
    -DBeaver
    -Docker PostgreSQL

## DB - Postgres

For running the database we must do in a terminal:
    
    -docker pull postgres
    -docker run --name members -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -p 5432:5432 postgres

Then in the database management we have to configure the connection to see the information in the tables.

## Migrations

To introduce some extra values in the database I chose to add Flyway DB to run SQL scripts and have data when we execute the endpoints. To run migrations, we have to run the following on a terminal: **mvn compile flyway:migrate**

## Endpoints

    -Create student: POST {localhost}/student 
    -Create asignature: POST {localhost}/student/asignature
    -Create subscription: POST {localhost}/student/subscription
    -Get all students: GET {localhost}/student/list
    -Get students sort by last name: GET {localhost}/student/sortlastname
    -Get students by subject: GET {localhost}/student/{subject}
    -Get students between 19 and 22: GET {localhost}/student/listage

Postman collection: [collection](https://gitlab.com/brian.hamer/school-java/-/blob/master/postman-collection/Java%20School.postman_collection.json) 

## Exercises

**A)** Domain Diagram could be found in the path: [ModelDomain](https://gitlab.com/brian.hamer/school-java/-/blob/master/diagrams/A-Model%20domain.png)

**B)** For this exercise I use 2 queries found in `IStudentRepository`. The first one, returns a list of string with the first letters of the surnames. Then for each of them I ask all the `students` that has that letter in the last name.

**Endpoint**: GET {localhost}/student/sortlastname

**C)** This exercise execute the following query to retrieve all the `students` in the `subscription` table that are related to a particular `subject`.

**Query**:findAllBySubjectId(Long subjectId).

**Endpoint**: GET {localhost}/student/{subject}.

Note: Taking into account that a `student` could not be repeated by design, I didn't have to make any modification. My `Subscription` table has the tuple (`IDStundent, IDSubject`) as its primary key, so this would prevent a student to subscribe twice to the same subject.

**D)** Both databases structure could be found in [Databases Structures](https://gitlab.com/brian.hamer/school-java/-/blob/master/diagrams/D-Databases.png) with their advantages and disadvantages.

**E)** New query proposed:

    SELECT name, last_name FROM Person p
    INNER JOIN Janitor j
    ON p.id = j.id
    WHERE j.workingArea = ‘Hallway’;

There is no need to inner join with the `employee` table because all the ids are there are the same.
Also we could create an index for the string `workingArea`, so it will map the id of "Hallway" with a unique hash.  

**F)** We can use Views. Facilitates the handling of large volumes of information, making them easier and faster to manipulate. We can make a View from a View, so we don't repeat redundant information.

**G)** To get faster the results I will implement a View: 

    CREATE VIEW StudentsBetween19And22 AS
        select * FROM Student s WHERE (extract (year from now()) - extract (year from s.birth_date)) BETWEEN 19 AND 22;

Then I only have to call it with:

    select * from StudentsBetween19And22

Results applying the view: 
    
    -       BirthDate      LastName        Name
    -1	2000-11-16	lopez	      brian
    -2	1998-11-16	hernandez     leandro
    -7	2000-11-16	carlos	      sebastian
    -8	2000-11-16	henriquez     marcos
    -9	2000-11-16	luro	      marcelo
    -10	2000-11-16	lara	      kevin
    -11	2000-11-16	mostaza	      federico
    -13	2001-11-16	sanchez       marta

**Endpoint**: GET {localhost}/student/listage

**H)** If we want to persist a student for example using the logic in the database I will use a Stored Procedure. This receives the student information and generate an insert it on the DB.
Having the logic in the database, changing the business logic becomes more complex, there are no collaborative work, you cannot do git operations on a Database for example.
I would not recommend this. However, I will use Stored Procedures.