package com.example.members.service;

import com.example.members.dto.DtoStudent;
import com.example.members.entities.Student;
import com.example.members.repository.IStudentRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.GregorianCalendar;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
class StudentServiceImplTest {

    @Mock
    IStudentRepository studentRepositoryMock;

    @InjectMocks
    StudentServiceImpl studentService;

    Student mockStudent;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);;
        mockStudent = new Student();
        mockStudent.setName("brian");
        mockStudent.setLastName("prueba");
        mockStudent.setBirthDate(new GregorianCalendar(2020, 11, 8).getTime());

        Student mockStudent = new Student();

        Mockito.when(studentRepositoryMock.save(mockStudent)).thenReturn(mockStudent);
    }

    @Test
    void saveStudent() {
        Student respuestaServicio;
        DtoStudent student = new DtoStudent();
        student.setName("nombre");
        student.setLastName("apellido");
        student.setBirthDate(new GregorianCalendar(2020, 11, 8).getTime());
        studentService.saveStudent(student) ;
        Mockito.verify(studentRepositoryMock).save(any(Student.class));
    }

}