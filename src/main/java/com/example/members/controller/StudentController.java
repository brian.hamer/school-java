package com.example.members.controller;

import com.example.members.dto.DtoSubscription;
import com.example.members.dto.DtoStudent;
import com.example.members.service.StudentServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.SortedMap;

@RestController
@RequestMapping("/student")
public class StudentController {

    public static final Log LOGGER = LogFactory.getLog(StudentController.class);

    @Autowired
    private StudentServiceImpl studentServiceImpl;

    @PostMapping
    public ResponseEntity<Void> addStudent(@Valid @RequestBody DtoStudent student){
        LOGGER.info("Call :: " + "addStudent()");
        this.studentServiceImpl.saveStudent(student);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @RequestMapping(value="list")
    public ResponseEntity<List<DtoStudent>> listStudents() {
        LOGGER.info("Call :: " + "listStudents()");
        return new ResponseEntity<List<DtoStudent>>(this.studentServiceImpl.listStudents() , HttpStatus.OK);
    }

    @PostMapping(value="subscription")
    public ResponseEntity<Void> addSubscription(@Valid @RequestBody DtoSubscription subscription) {
        try {
            LOGGER.info("Call :: " + "addSubscription()");
            this.studentServiceImpl.saveSubscription(subscription);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value="sortlastname")
    public ResponseEntity<SortedMap<String, List<DtoStudent>>> sortByLastName() {
        LOGGER.info("Call :: " + "findSortStudents()");
        return new ResponseEntity<SortedMap<String, List<DtoStudent>>>(this.studentServiceImpl.sortStudents() , HttpStatus.OK);
    }

    @RequestMapping(value="{subject}")
    public ResponseEntity<List<DtoStudent>> getStudentsBySubject(@PathVariable("subject") String subject) {
        LOGGER.info("Call :: " + "findStudentsBySubject()");
        return new ResponseEntity<List<DtoStudent>>(this.studentServiceImpl.findStudentsBySubject(subject) , HttpStatus.OK);
    }

    @RequestMapping(value="listage")
    public ResponseEntity<List<DtoStudent>> getStudentsByAge() {
        LOGGER.info("Call :: " + "findStudentsByAge()");
        return new ResponseEntity<List<DtoStudent>>(this.studentServiceImpl.findStudentsByAge() , HttpStatus.OK);
    }
}