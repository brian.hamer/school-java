package com.example.members.controller;

import com.example.members.dto.DtoSubject;
import com.example.members.entities.Subject;
import com.example.members.exception.SubjectAlreadyExistsException;
import com.example.members.service.AsignatureServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/asignature")
public class AsginatureController {

    public static final Log LOGGER = LogFactory.getLog(AsginatureController.class);

    @Autowired
    private AsignatureServiceImpl asignatureService;

    @PostMapping
    public ResponseEntity<DtoSubject> addAsignature(@RequestBody Subject asignature, HttpServletResponse response) {
        LOGGER.info("Call :: " + "addAsignature()");
        try {
            return new ResponseEntity<DtoSubject>(this.asignatureService.saveAsignature(asignature), HttpStatus.CREATED);
        } catch (SubjectAlreadyExistsException ex) {
            throw new ResponseStatusException(HttpStatus.ALREADY_REPORTED, "Subject already exists", ex);
        }
    }

}
