package com.example.members.entities;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"ASIGNATURE"})})
public class Subject {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ASIGNATURE")
    private String asignature;

    @OneToMany(mappedBy = "subject")
    Set<Subscription> subscriptions;

    public Subject(){}

    public Subject(Long id, String asignature, Set<Subscription> subscriptions) {
        this.id = id;
        this.asignature = asignature;
        this.subscriptions = subscriptions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAsignature() {
        return asignature;
    }

    public void setAsignature(String asignature) {
        this.asignature = asignature;
    }

    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Set<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
}
