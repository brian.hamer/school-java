package com.example.members.dto;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class DtoStudent {

    @NotEmpty(message = "{name.notEmpty}")
    private String name;

    @NotEmpty(message = "{lastName.notEmpty}")
    private String lastName;


    @NotNull(message = "{birthDate.notEmpty}")
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
