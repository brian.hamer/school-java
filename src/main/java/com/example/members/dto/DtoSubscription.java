package com.example.members.dto;

public class DtoSubscription {

    private Long stuId;

    private Long subId;

    public Long getStuId() {
        return stuId;
    }

    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    public Long getSubject() {
        return subId;
    }

    public void setSubject(Long subject) {
        this.subId = subject;
    }
}
