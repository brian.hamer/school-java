package com.example.members.service;

import com.example.members.entities.Student;
import com.example.members.dto.DtoStudent;
import com.example.members.dto.DtoSubscription;

import java.util.List;

public interface IStudentService {
    public abstract Student saveStudent(DtoStudent student);
    public abstract List<Student> listStudents();
    public abstract List<Student> findStudentsBySubject();
    public abstract List<Student> findStudentsByAge();
    public void saveSubscription(DtoSubscription subscription);
}
