package com.example.members.service;

import com.example.members.dto.DtoSubject;
import com.example.members.entities.Subject;
import com.example.members.exception.SubjectAlreadyExistsException;
import com.example.members.repository.ISubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AsignatureServiceImpl {

    @Autowired
    private ISubjectRepository repository;

    public DtoSubject saveAsignature(Subject asignature)  {
        try {
            this.repository.save(asignature);
            DtoSubject result = new DtoSubject();
            result.setSubject(asignature.getAsignature());
            return result;
        } catch (SubjectAlreadyExistsException ex) {
        throw new SubjectAlreadyExistsException("Subject already exists");
    }
    }

}




