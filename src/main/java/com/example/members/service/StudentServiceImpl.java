package com.example.members.service;

import com.example.members.entities.Student;
import com.example.members.entities.Subject;
import com.example.members.dto.DtoStudent;
import com.example.members.dto.DtoSubscription;
import com.example.members.entities.Subscription;
import com.example.members.entities.SubscriptionKey;
import com.example.members.repository.IStudentRepository;
import com.example.members.repository.ISubjectRepository;
import com.example.members.repository.ISubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class StudentServiceImpl {

    @Autowired
    private IStudentRepository studentRepository;

    @Autowired
    private ISubjectRepository subjectRepository;

    @Autowired
    private ISubscriptionRepository subscriptionRepository;


    public void saveStudent(DtoStudent student) {
        Student studentSave = new Student();
        studentSave.setName(student.getName());
        studentSave.setLastName(student.getLastName());
        studentSave.setBirthDate(student.getBirthDate());
        this.studentRepository.save(studentSave);
    }

    public List<DtoStudent> listStudents() {
        List<DtoStudent> result = new ArrayList<DtoStudent>();
       List<Student> list = this.studentRepository.findAll();
       for (Student s: list) {
           DtoStudent dto = new DtoStudent();
           dto.setName(s.getName());
           dto.setLastName(s.getLastName());
           dto.setBirthDate(s.getBirthDate());
           result.add(dto);
       }
       return result;
    }

    public SortedMap<String, List<DtoStudent>> sortStudents() {
        SortedMap<String, List<DtoStudent>> result = new TreeMap<>();
        List<String> studentsLastNameLetters = this.studentRepository.studentsLastNameLetters();
        for (String s: studentsLastNameLetters) {
            List<Student> students = this.studentRepository.findStudentsByLetter(s);
            List<DtoStudent> resultDto = new ArrayList<DtoStudent>();
            for (Student st: students){
                DtoStudent dto = new DtoStudent();
                dto.setName(st.getName());
                dto.setLastName(st.getLastName());
                dto.setBirthDate(st.getBirthDate());
                resultDto.add(dto);
            }
            result.put(s,resultDto);
        }
        return result;
    }

    public List<DtoStudent> findStudentsByAge() {
        List<DtoStudent> result = new ArrayList<DtoStudent>();
        List<Student> students =this.studentRepository.findAllByAge();
        for (Student s: students) {
            DtoStudent dtoStu = new DtoStudent();
            dtoStu.setName(s.getName());
            dtoStu.setLastName(s.getLastName());
            dtoStu.setBirthDate(s.getBirthDate());
            result.add(dtoStu);
        }
        return result;
    }

    public void saveSubscription(DtoSubscription subscription) {
        Subscription subs = new Subscription();
        Optional<Student> student = this.studentRepository.findById(subscription.getStuId());
        if (student.isPresent()) {
            Optional<Subject> subject = this.subjectRepository.findById(subscription.getSubject());
            if (subject.isPresent()) {
                SubscriptionKey key = new SubscriptionKey();
                key.setStudentId(student.get().getId());
                key.setSubjectId(subject.get().getId());
                subs.setId(key);
                subs.setStudent(student.get());
                subs.setSubject(subject.get());
                subs.setRegisteredAt(LocalDateTime.now());
                this.subscriptionRepository.save(subs);
            }
        }
    }


    public List<DtoStudent> findStudentsBySubject(String subject){
        Subject sub = this.subjectRepository.findByAsignature(subject);
        List<Subscription> subscriptions = this.subscriptionRepository.findAllBySubjectId(sub.getId());
        List<DtoStudent> students = new ArrayList<DtoStudent>();
        for (Subscription subs : subscriptions) {
            Optional<Student> stu = this.studentRepository.findById(subs.getStudent().getId());
            if (stu.isPresent()) {
                DtoStudent dto = new DtoStudent();
                dto.setName(stu.get().getName());
                dto.setLastName(stu.get().getLastName());
                dto.setBirthDate(stu.get().getBirthDate());
                students.add(dto);
            }
        }
        return students;
    }

}
