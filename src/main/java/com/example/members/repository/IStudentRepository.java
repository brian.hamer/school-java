package com.example.members.repository;

import com.example.members.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface IStudentRepository extends JpaRepository<Student, Long> {

    @Query("select UPPER(SUBSTRING(lastName, 1, 1)) as letter from Student GROUP BY SUBSTRING(lastName, 1, 1) order by letter")
    public List<String> studentsLastNameLetters();

    @Query("select s from Student s where UPPER(SUBSTRING(s.lastName, 1, 1)) = UPPER(?1)")
    public List<Student> findStudentsByLetter(String s);

    //@Query("select s from Student s where (extract (year from now()) - extract (year from s.birthDate)) BETWEEN 19 AND 22")
    @Query(nativeQuery = true, value ="select * from StudentsBetween19And22")
    public List<Student> findAllByAge();
}
