package com.example.members.repository;

import com.example.members.entities.Subscription;
import com.example.members.entities.SubscriptionKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISubscriptionRepository extends JpaRepository<Subscription, SubscriptionKey> {
    List<Subscription> findAllBySubjectId(Long subjectId);
}
