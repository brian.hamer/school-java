package com.example.members.repository;

import com.example.members.entities.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISubjectRepository extends JpaRepository<Subject, Long> {
    Subject findByAsignature(String asignature);
}
