package com.example.members.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.ALREADY_REPORTED, reason = "Subscription already exists")
public class SubscriptionAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 1L;

    public SubscriptionAlreadyExistsException(String errorMessage) {
            super(errorMessage);
        }
}

