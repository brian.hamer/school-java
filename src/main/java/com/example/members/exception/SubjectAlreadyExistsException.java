package com.example.members.exception;

public final class SubjectAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = -632828666153643936L;

    public SubjectAlreadyExistsException(String message) {
        super(message);
    }
}

